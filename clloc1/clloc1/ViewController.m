//
//  ViewController.m
//  clloc1
//
//  Created by CLI112 on 10/26/15.
//  Copyright (c) 2015 CLI112. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>
@interface ViewController ()

{
    
    CLLocationManager *manager;
    
}
   
@property (strong, nonatomic) IBOutlet UILabel *label1;

@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UITextField *text1;
@property (strong, nonatomic) IBOutlet UITextField *text2;

@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet MKMapView *map;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
      [manager startUpdatingLocation];
    [manager requestWhenInUseAuthorization];


    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)click:(id)sender {
    [manager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
       /*self.text1.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
      self.text2.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];*/
        MKPointAnnotation *annotation=[[MKPointAnnotation alloc]init];
        annotation.coordinate=currentLocation.coordinate;
        annotation.title=@"here";
        annotation.subtitle=@"u are";
        [_map addAnnotation:annotation];
    }
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
